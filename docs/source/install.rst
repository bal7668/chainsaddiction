Installation
=======================================

Clone this repository, change to its root directory and issue

.. code-block:: bash

    pip install .

On macOS this should work perfectly find. On GNU/Linux it might happend that
you see an error related to missing a Python.h file. If you see such an error,
you probabliy missing you distributions Python development package. In most
cases this package is named either `python3-dev` or `python3-devel`. You need
to install it with your package manager. On Fedora, for example, this would
look like:

.. code-block:: bash

    sudo yum install python3-devel
